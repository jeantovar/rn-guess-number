import React from 'react';
import { View, Text, StyleSheet, TextInput, Button } from 'react-native';
import Card from '../components/Card';

const StartGameScreen = props => {
    return (
        <View style={styles.screen}>
            <Text style={styles.title}>Start a new Game!</Text>
            <Card style={styles.inputBox}>
                <TextInput placeholder="Select a Number" />
                <View style={styles.buttonBox}>
                    <Button title="Reset" onPress={()=> {console.log("Reset")}}/>
                    <Button title="Confirm" onPress={()=> {console.log("Confirm")}}/>
                </View>
            </Card>
            
        </View>
    );
}

const styles = StyleSheet.create({
    screen :{
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        padding: 10
    },
    buttonBox: {
        flexDirection: "row",
        width: "100%",
        paddingHorizontal: 15,
        justifyContent: 'space-between'
    },
    title:{
        fontSize: 20,
        marginVertical: 10
    },
    inputBox: {
        width: "80%",
        alignItems: "center",
        
    }
}); 

export default StartGameScreen;